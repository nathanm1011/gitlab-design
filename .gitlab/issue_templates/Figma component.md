<!-- Start by making a copy of the component template to your own drafts.

In Figma:
1. Duplicate the component template https://www.figma.com/file/OmvFfWkqEsdGhXAND133ou/Component?node-id=0%3A1
2. Open the duplicate, then use the dropdown next to the file name to select “Move to Project…”
   and select your Drafts as the new location.
3. Update the template with your component name and start designing ;)
-->

<!--Add a short description of the component. If it’s helpful, add a checklist of variations
and states to the description so that a reviewer can be sure to cross reference everything
that has been completed.-->

<!--Use the Figma share feature and make sure that “anyone with the link” can view. Then, 
specifically invite the reviewer with “edit” permissions selected. Anyone can duplicate the 
file to their own drafts and edit from there, but the reviewer can directly edit and 
collaborate on the file. This will help maintain the integrity of the initial draft.-->

[View component in Figma →](ADD LINK TO FIGMA FRAME)

### Checklist

Make sure the following are completed before closing the issue:

1. [ ] **Assignee**: Create component in your own draft file in Figma using the
[component template](https://www.figma.com/file/OmvFfWkqEsdGhXAND133ou/%5BComponent%5D),
including all variations and states.
1. [ ] **Assignee**: Ask another Figma Pilot Designer to review your component (make sure they have edit permissions).
1. [ ] **Reviewer**: Review and approve assignee’s addition. Ensure that component
matches Sketch specs, includes all current variations and states, and (if applicable)
is responsive.
1. [ ] **Assignee**: Add notes about the component construction to the 
[Figma component build notes](https://gitlab.com/gitlab-org/gitlab-design/issues/778)
issue description. Check off the component when complete.
1. [ ] **Assignee**: Add the component to the **Globals - beta** file, and view
the component in the Assets panel to ensure it aligns with what’s outlined in the
[document and asset library structure](https://gitlab.com/gitlab-org/gitlab-design/-/blob/master/CONTRIBUTING-Figma.md#document-and-asset-library-structure) documentation.
1. [ ] **Assignee**: Publish the library changes along with a clear commit message.
1. [ ] **Assignee**: Update component link in the issue.
1. [ ] **Assignee**: Move your draft file to the **Component archive** Figma project.

/label ~"UX" ~"Figma"
